package dk.danskespil.email.simple;

import javax.mail.*;
import javax.mail.internet.*;
import java.util.Collection;
import java.util.Date;

public class JavaMailMailer implements MailWrapper {

    private final Session session;

    public JavaMailMailer(Session session) {
        this.session = session;

    }

    private static Address[] toAddressArray(Collection<String> adresses) {
        if (adresses == null || adresses.size() == 0) {
            return null;
        }
        Address[] addrs = new Address[adresses.size()];
        int i = 0;
        for (String addr : adresses) {
            try {
                addrs[i++] = new InternetAddress(addr, false);
            } catch (AddressException e) {
                throw new MailWrapperException(e);
            }
        }
        return addrs;
    }

    @Override
    public void sendEmail(Mail mail) {
        MimeMessage msg = new MimeMessage(session);

        try {
            msg.setFrom(mail.getFrom());

            msg.setSubject(mail.getSubject(), "UTF-8");

            String plain = mail.getBodyText();
            MimeBodyPart textPart = null;
            if (plain != null && plain.trim().length() > 0) {
                textPart = new MimeBodyPart();
                textPart.setContent(plain, "text/plain");
            }

            String html = mail.getBodyText();
            final MimeBodyPart htmlPart = new MimeBodyPart();
            htmlPart.setContent(mail.getBodyHtml(), "text/html");

            final Multipart mp = new MimeMultipart("alternative");
            mp.addBodyPart(textPart);
            mp.addBodyPart(htmlPart);

            msg.setContent(mp);
            msg.setSentDate(new Date());
            msg.setRecipients(Message.RecipientType.TO, toAddressArray(mail.getTo()));
            msg.setRecipients(Message.RecipientType.CC, toAddressArray(mail.getCc()));
            msg.setRecipients(Message.RecipientType.BCC, toAddressArray(mail.getBcc()));
            Transport.send(msg);
        } catch (MessagingException e) {
            throw new MailWrapperException(e);
        }

    }
}

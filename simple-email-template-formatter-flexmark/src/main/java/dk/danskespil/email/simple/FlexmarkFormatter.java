package dk.danskespil.email.simple;

import com.vladsch.flexmark.ast.Document;
import com.vladsch.flexmark.ext.tables.TablesExtension;
import com.vladsch.flexmark.ext.yaml.front.matter.AbstractYamlFrontMatterVisitor;
import com.vladsch.flexmark.ext.yaml.front.matter.YamlFrontMatterExtension;
import com.vladsch.flexmark.html.HtmlRenderer;
import com.vladsch.flexmark.parser.Parser;
import com.vladsch.flexmark.util.options.MutableDataSet;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Uses MarkDown as primary (plain-text) format and convert to HTML.
 */
public class FlexmarkFormatter implements FormatterWrapper<Map<String, List<String>>> {

    private String content;
    private Map<String, List<String>> frontMatter;

    @Override
    public String getHtml() {
        Objects.requireNonNull(content, "content is null, did you forget to call setContent(...)?");
        MutableDataSet options = new MutableDataSet()
                .set(HtmlRenderer.INDENT_SIZE, 2)
                .set(HtmlRenderer.PERCENT_ENCODE_URLS, true)
                .set(Parser.EXTENSIONS, Arrays.asList(TablesExtension.create(), YamlFrontMatterExtension.create()));
        Parser parser = Parser.builder(options).build();

        Document document = parser.parse(content);
        HtmlRenderer renderer = HtmlRenderer.builder(options).indentSize(2).build();
        String html = renderer.render(document);
        AbstractYamlFrontMatterVisitor frontMatterVisitor = new AbstractYamlFrontMatterVisitor();
        frontMatterVisitor.visit(document);
        frontMatter = frontMatterVisitor.getData();
        return html;
    }

    @Override
    public String getPlainText() {
        Objects.requireNonNull(content, "content is null, did you forget to call setContent(...)?");
        return content;
    }

    @Override
    public Map<String, List<String>> getFrontMatter() {
        return frontMatter;
    }

    @Override
    public void setContent(String content) {
        Objects.requireNonNull(content);
        this.content = content;
    }

}

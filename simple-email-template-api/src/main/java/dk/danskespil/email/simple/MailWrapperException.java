package dk.danskespil.email.simple;

public class MailWrapperException extends RuntimeException {
    public MailWrapperException() {
        super();
    }

    public MailWrapperException(Exception cause) {
        super(cause);
    }

}

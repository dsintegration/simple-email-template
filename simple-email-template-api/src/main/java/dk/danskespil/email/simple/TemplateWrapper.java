package dk.danskespil.email.simple;

import java.util.Map;

/**
 * TemplateWrapper wraps a template engine.
 */
public interface TemplateWrapper {

    void fetchTemplate(String resource);

    String processTemplate(Map<String, Object> data);
}

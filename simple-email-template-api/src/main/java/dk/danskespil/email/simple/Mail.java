package dk.danskespil.email.simple;

import lombok.Builder;
import lombok.Data;

import java.util.Collection;

@Builder
@Data
public class Mail {
    private String subject;
    private String bodyHtml;
    private String bodyText;
    private Collection<String> to;
    private Collection<String> cc;
    private Collection<String> bcc;
    private String from;
}

package dk.danskespil.email.simple;

/**
 * Wraps HTML &lt;-&gt; Plain text conversion engine.
 */
public interface FormatterWrapper<T> {

    void setContent(String content);

    String getHtml();

    String getPlainText();

    T getFrontMatter();

}

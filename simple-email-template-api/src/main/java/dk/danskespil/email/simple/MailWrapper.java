package dk.danskespil.email.simple;

/**
 * MailWrapper wraps a mail sending implementation like JavaMail or Aws-Ses.
 */
public interface MailWrapper {
    void sendEmail(Mail mail);
}

package dk.danskespil.email.simple;

import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.model.*;

import java.nio.charset.StandardCharsets;

public class AwsSesMailer implements MailWrapper {
    private final AmazonSimpleEmailService amazonSimpleEmailService;

    public AwsSesMailer(AmazonSimpleEmailService amazonSimpleEmailService) {
        this.amazonSimpleEmailService = amazonSimpleEmailService;
    }

    @Override
    public void sendEmail(Mail mail) {
        Body body = new Body()
                .withHtml(getContent(mail.getBodyHtml()))
                .withText(getContent(mail.getBodyText()));
        Content subject = getContent(mail.getSubject());
        Message msg = new Message()
                .withBody(body)
                .withSubject(subject);
        Destination destination = new Destination()
                .withToAddresses(mail.getTo())
                .withCcAddresses(mail.getCc())
                .withBccAddresses(mail.getBcc());
        SendEmailRequest sendEmailRequest = new SendEmailRequest().withMessage(msg).withDestination(destination);
        amazonSimpleEmailService.sendEmail(sendEmailRequest);
    }

    private Content getContent(String str) {
        return new Content(str).withCharset(StandardCharsets.UTF_8.name());
    }
}
